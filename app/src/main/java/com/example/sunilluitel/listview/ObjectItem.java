package com.example.sunilluitel.listview;

/**
 * Created by sunilluitel on 8/17/16.
 */
/**
 * Modified by sunildhakal on 2/9/23.
 */
public class ObjectItem {
    public int itemId;
    public String itemName;
    public int[] itemProperty;
    public int[] itemProperty1;

    // constructor
    public ObjectItem(int itemId, String itemName, int[] itemProperty, int[] itemProperty1) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemProperty = itemProperty;
        this.itemProperty1 = itemProperty1;
    }
}
